'use strict';

/**
 * @ngdoc function
 * @name myprofileangApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the myprofileangApp
 */
angular.module('myprofileangApp')
  .controller('ContactCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
