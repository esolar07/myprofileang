'use strict';

/**
 * @ngdoc function
 * @name myprofileangApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the myprofileangApp
 */
angular.module('myprofileangApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
